require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "didaas-mobile-agent"
  s.version      = package["version"]
  s.summary      = "Some"
  s.description  = <<-DESC
                didaas-mobile-agent
                   DESC
  s.homepage     = "https://github.com/github_account/react-native-my-fancy-library"
  s.license      = "MIT"
  # s.license    = { :type => "MIT", :file => "FILE_LICENSE" }
  s.authors      = { "AWTS" => "info@ayanworks.com" }
  s.platforms    = { :ios => "9.0" }
  s.source       = { :git => "https://github.com/github_account/react-native-my-fancy-library.git", :tag => "#{s.version}" }

  s.source_files = "ios/**/*.{h,m,swift}"
  s.requires_arc = true

  s.dependency "React"
  s.dependency 'libindy'
  s.dependency 'libindy-objc', '~> 1.8.2'
  s.dependency 'libsodium',"~> 1.0.12"
  # ...
  # s.dependency "..."
end

