
# react-native-didaas-mobile-agent

## Getting started

`$ npm install react-native-didaas-mobile-agent --save`

### Mostly automatic installation

`$ react-native link react-native-didaas-mobile-agent`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-didaas-mobile-agent` and add `RNDidaasMobileAgent.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNDidaasMobileAgent.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNDidaasMobileAgentPackage;` to the imports at the top of the file
  - Add `new RNDidaasMobileAgentPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-didaas-mobile-agent'
  	project(':react-native-didaas-mobile-agent').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-didaas-mobile-agent/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-didaas-mobile-agent')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNDidaasMobileAgent.sln` in `node_modules/react-native-didaas-mobile-agent/windows/RNDidaasMobileAgent.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Didaas.Mobile.Agent.RNDidaasMobileAgent;` to the usings at the top of the file
  - Add `new RNDidaasMobileAgentPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNDidaasMobileAgent from 'react-native-didaas-mobile-agent';

// TODO: What to do with the module?
RNDidaasMobileAgent;
```
  