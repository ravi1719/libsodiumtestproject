#import "RNDidaasMobileAgent.h"
#import <React/RCTBridge.h>
#import <Indy/Indy.h>

@implementation RNDidaasMobileAgent

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(createWallet:
                  (NSString *)config
                  :(NSString *)cred
                  resolve:(RCTPromiseResolveBlock)resolve
                  reject:(RCTPromiseRejectBlock)reject) {
    [[IndyWallet sharedInstance] createWalletWithConfig:config credentials:cred completion:^(NSError *error) {
        if(error.code > 1) {
            NSLog(@"did%@", error.localizedDescription);
            reject(@(-1).stringValue, error.localizedDescription, nil);
        }
        else {
            resolve(@"Wallet created successfully");
            NSLog(@"created");
        }
    }];
}

@end
