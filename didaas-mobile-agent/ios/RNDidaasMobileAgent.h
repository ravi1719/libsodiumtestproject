#import <React/RCTBridgeModule.h>

@interface RNDidaasMobileAgent : NSObject <RCTBridgeModule>
- (void) sendEvent:(NSString *_Nullable)msg params:(NSDictionary *_Nullable)params;
- (NSInteger) metadataMaxSize;
- (NSData *_Nullable)readyToSendMetadataAtTimestamp:(NSTimeInterval)timestamp;
- (void)receiveMetadata:(NSData *_Nonnull)data fromUser:(NSInteger)uid atTimestamp:(NSTimeInterval)timestamp;
@end
