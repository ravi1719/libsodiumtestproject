####Steps to run project
 
1. After cloning this repo go inside `LibSodiumTestProject` in your directory and hit
```
npm install
```
- Note if you dont have `npm` installed already try installing with homebrew using `brew install node`.

2. Now inside terminal from root of this project go to `cd LibSodiumTestProject/ios/`

3. Now hit `pod install`

4. This will install all the iOS dependencies and a `LibSodiumTestProject.xcworkspace` file will be generated.

5. Double click the file to open in Xcode and build it. It should build successfully.



####Steps to reproduce the issue

1. Go inside `LibSodiumTestProject/ios/` folder.

2. Copy `didaas-mobile-agent` folder from root folder to `node_modules` folder.

3. Open the `Podfile` and edit the commented line at `line no: 58`.

4. Now delete `Pods` folder and inside your terminal hit `pod install` inside the ios directory.

5. Now again try to build the project.

6. Now the errors will occur which we wre facing currently.